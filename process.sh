#!/bin/bash
#
# Script to process PCAP files suitable for consumption into DataTable
#

process() {
  declare_fieldsets

  # all layer 3 data
  output_processing $1 layer3.out "-Y ip" ${packet_meta[@]} ${layer2_core[@]} ${layer3_core[@]} ${layer3_detail[@]}
  prefix_file layer3.out "$1,"
  
  # all UDP
  output_processing $1 layer4-UDP.out "-Y udp" ${packet_meta[@]} ${layer2_core[@]} ${layer3_core[@]} ${layer4_UDP_core[@]} ${layer4_UDP_detail[@]}
  prefix_file layer4-UDP.out "$1,"
  
  # all TCP
  output_processing $1 layer4-TCP.out "-Y tcp" ${packet_meta[@]} ${layer2_core[@]} ${layer3_core[@]} ${layer4_TCP_core[@]} ${layer4_TCP_detail[@]}
  prefix_file layer4-TCP.out "$1,"     

  # all HTTP
  output_processing $1 layer7-TCP-HTTP.out "-Y http" ${packet_meta[@]} ${layer2_core[@]} ${layer3_core[@]} ${layer4_TCP_core[@]} ${layer7_TCP_HTTP_detail[@]}
  prefix_file layer4-TCP.out "$1,"   
}


declare_fieldsets() {
	# * = to be dereferenced
	
	# basic packet metadata
	packet_meta=( 
		#*filename
		#*packet identifier
		frame.time_epoch
	)
	
	# Data link layer (2)
	layer2_core=(
		#*MAC
	)
	
	# Network layer (3)
	layer3_core=(
		ip.src
		ip.dst
		ip.len
		ip.proto
	)
	layer3_detail=(
		ip.ttl
		ip.checksum
	)
	
	# Transport layer (4) - UDP (17)
	layer4_UDP_core=(
		udp.srcport
		udp.dstport
		udp.length
	)
	layer4_UDP_detail=(
		udp.checksum
		udp.stream # Stream indexes are Wireshark-internal.
	)
	
	# Transport layer (4) - TCP (6)
	layer4_TCP_core=(
		tcp.srcport
		tcp.dstport
		tcp.seq
		tcp.len
	)
	layer4_TCP_detail=(
		tcp.checksum
		tcp.window_size
		tcp.stream # Stream indexes are Wireshark-internal.  http://stackoverflow.com/questions/6076897/follow-tcp-stream-where-does-field-stream-index-come-from
		tcp.ack
		tcp.flags.res
		tcp.flags.ns
		tcp.flags.cwr
		tcp.flags.ecn
		tcp.flags.urg
		tcp.flags.ack
		tcp.flags.push
		tcp.flags.reset
		tcp.flags.syn
		tcp.flags.fin
	)
	
  layer7_TCP_HTTP_detail=(
#   http.notification # Notification FT_BOOLEAN http  0 0x0 TRUE if HTTP notification
#   http.response # Response FT_BOOLEAN http  0 0x0 TRUE if HTTP response
#   http.request # Request FT_BOOLEAN http  0 0x0 TRUE if HTTP request
#   http.response_number # Response number FT_UINT32  http  BASE_DEC  0x0
#   http.request_number # Request number FT_UINT32  http  BASE_DEC  0x0
    http.authbasic # Credentials FT_STRING  http    0x0
#   http.response.line # Response line FT_STRING  http    0x0
    http.request.line # Request line FT_STRING  http    0x0
    http.request.method # Request Method FT_STRING  http    0x0 HTTP Request Method
    http.request.uri # Request URI FT_STRING  http    0x0 HTTP Request-URI
#   http.request.version # Request Version FT_STRING  http    0x0 HTTP Request HTTP-Version
#   http.request.full_uri # Full request URI FT_STRING  http    0x0 The full requested URI (including host name)
    http.response.code # Status Code FT_UINT16  http  BASE_DEC  0x0 HTTP Response Status Code
#   http.response.phrase # Response Phrase FT_STRING  http    0x0 HTTP Response Reason Phrase
#   http.authorization # Authorization FT_STRING  http    0x0 HTTP Authorization header
#   http.proxy_authenticate # Proxy-Authenticate FT_STRING  http    0x0 HTTP Proxy-Authenticate header
#   http.proxy_authorization # Proxy-Authorization FT_STRING  http    0x0 HTTP Proxy-Authorization header
#   http.proxy_connect_host # Proxy-Connect-Hostname FT_STRING  http    0x0 HTTP Proxy Connect Hostname
#   http.proxy_connect_port # Proxy-Connect-Port FT_UINT16  http  BASE_DEC  0x0 HTTP Proxy Connect Port
#   http.www_authenticate # WWW-Authenticate FT_STRING  http    0x0 HTTP WWW-Authenticate header
    http.content_type # Content-Type FT_STRING  http    0x0 HTTP Content-Type header
#   http.content_length_header # Content-Length FT_STRING http    0x0 HTTP Content-Length header
    http.content_length # Content length FT_UINT64  http  BASE_DEC  0x0
#   http.content_encoding # Content-Encoding FT_STRING  http    0x0 HTTP Content-Encoding header
#   http.transfer_encoding # Transfer-Encoding FT_STRING  http    0x0 HTTP Transfer-Encoding header
#   http.upgrade # Upgrade FT_STRING  http    0x0 HTTP Upgrade header
    http.user_agent # User-Agent FT_STRING  http    0x0 HTTP User-Agent header
#   http.host # Host FT_STRING  http    0x0 HTTP Host
#   http.connection # Connection FT_STRING  http    0x0 HTTP Connection
    http.cookie # Cookie FT_STRING  http    0x0 HTTP Cookie
#   http.cookie_pair # Cookie pair FT_STRING  http    0x0 A name/value HTTP cookie pair
#   http.accept # Accept FT_STRING  http    0x0 HTTP Accept
#   http.referer # Referer FT_STRING  http    0x0 HTTP Referer
#   http.accept_language # Accept-Language FT_STRING  http    0x0 HTTP Accept Language
#   http.accept_encoding # Accept Encoding FT_STRING  http    0x0 HTTP Accept Encoding
#   http.date # Date FT_STRING  http    0x0 HTTP Date
#   http.cache_control # Cache-Control FT_STRING  http    0x0 HTTP Cache Control
#   http.server # Server FT_STRING  http    0x0 HTTP Server
#   http.location # Location FT_STRING  http    0x0 HTTP Location
#   http.sec_websocket_accept # Sec-WebSocket-Accept FT_STRING  http    0x0
#   http.sec_websocket_extensions # Sec-WebSocket-Extensions FT_STRING  http    0x0
#   http.sec_websocket_key # Sec-WebSocket-Key FT_STRING  http    0x0
#   http.sec_websocket_protocol # Sec-WebSocket-Protocol FT_STRING  http    0x0
#   http.sec_websocket_version # Sec-WebSocket-Version FT_STRING  http    0x0
#   http.set_cookie # Set-Cookie FT_STRING  http    0x0 HTTP Set Cookie
#   http.last_modified # Last-Modified FT_STRING  http    0x0 HTTP Last Modified
#   http.x_forwarded_for # X-Forwarded-For FT_STRING  http    0x0 HTTP X-Forwarded-For
#   http.request_in # Request in frame FT_FRAMENUM  http    0x0 This packet is a response to the packet with this number
#   http.response_in # Response in frame FT_FRAMENUM  http    0x0 This packet will be responded in the packet with this number
#   http.next_request_in # Next request in frame FT_FRAMENUM  http    0x0 The next HTTP request starts in packet number
#   http.next_response_in # Next response in frame FT_FRAMENUM  http    0x0 The next HTTP response starts in packet number
#   http.prev_request_in # Prev request in frame FT_FRAMENUM  http    0x0 The previous HTTP request starts in packet number
#   http.prev_response_in # Prev response in frame FT_FRAMENUM  http    0x0 The previous HTTP response starts in packet number
    http.time # Time since request FT_RELATIVE_TIME http    0x0 Time since the request was sent
#   http.chunked_trailer_part # trailer-part FT_STRING  http    0x0 Optional trailer in a chunked body
#   http.chunk_boundary # Chunk boundary FT_BYTES http    0x0
#   http.chunk_size # Chunk size FT_UINT32  http  BASE_DEC  0x0
#   http.unknown_header # Unknown header FT_STRING  http    0x0
#   http.chat # Expert Info FT_NONE http    0x0 Formatted text
#   http.chunkd_and_length # Expert Info FT_NONE  http    0x0 It is incorrect to specify a content-length header and chunked encoding together.
#   http.subdissector_failed # Expert Info FT_NONE  http    0x0 HTTP body subdissector failed, trying heuristic subdissector
#   http.ssl_port # Expert Info FT_NONE http    0x0 Unencrypted HTTP protocol detected over encrypted port, could indicate a dangerous misconfiguration.
#   http.leading_crlf # Expert Info FT_NONE http    0x0 Leading CRLF previous message in the stream may have extra CRLF
   )
	
}
prefix_file() {
  FILE_TO_PARSE=$1; shift
  VALUE=$1; shift
  
  sed -i "" "s/^/$VALUE/" $FILE_TO_PARSE
  
}

output_processing() {
	FILE_TO_PARSE=$1; shift
  OUTPUT_FILE=$1; shift
  OTHER_FLAGS=$1; shift   	
	FIELDS=""

	while [ -n "$1" ]; do
	  FIELDS="$FIELDS -e $1"
	  shift
	done
	
	# https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10220
	
	OPTIONS=(
    -r $FILE_TO_PARSE
    -n # no host lookup
    # -o wlan.enable_decryption:TRUE -o "uat:80211_keys:\"wpa-pwd\",\"subnet16121930:dd-wrt2\"" # temp for T4s wifi PCAP
    $OTHER_FLAGS 
    -T fields $FIELDS 
    -E header=n
    -E separator=,
    -E occurrence=a # all occurrences of a field (multi)
    -E aggregator=/s # multi-separator, /t tab, /s space, <someChar>
    -E quote=n # d uses double-quotes, s single-quotes, n no quotes (the default).
    -t ud
	)
	
	echo "tshark ${OPTIONS[@]} > $OUTPUT_FILE"
	tshark ${OPTIONS[@]} > $OUTPUT_FILE
	echo Done.
}

process $1