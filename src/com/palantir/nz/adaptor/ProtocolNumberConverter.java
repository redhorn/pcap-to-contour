package com.palantir.nz.adaptor;
import java.util.Map;

import com.google.common.collect.Maps;

public class ProtocolNumberConverter {
    
    // http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
    // 2015-10-06
    
    private final int protocolNumber;
    private final String keyword;
    private final String description;

    private ProtocolNumberConverter(int protocolNumber, String keyword, String description) {
        super();
        this.protocolNumber = protocolNumber;
        this.keyword = keyword;
        this.description = description;
    }

    public int getProtocolNumber() {
        return protocolNumber;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getDescription() {
        return description;
    }
    
    // public static stuff

    public static String lookupNameByNumber(Integer number) {
        ProtocolNumberConverter pn = lookupByNumber(number);
        return pn != null ? pn.getKeyword() : "";
    }
    public static ProtocolNumberConverter lookupByNumber(Integer number) {
        return numberToObject.get(number);
    }
    
    public static ProtocolNumberConverter lookupByKeyword(String keyword) {
        return keywordToObject.get(keyword.toLowerCase());
    }
    
    //private static
    
    private static Map<Integer, ProtocolNumberConverter> numberToObject = Maps.newHashMapWithExpectedSize(256);
    private static Map<String, ProtocolNumberConverter> keywordToObject = Maps.newHashMapWithExpectedSize(139); // no empty strings included
    
    private static void add(int protocolNumber, String keyword, String description) {
        ProtocolNumberConverter pn = new ProtocolNumberConverter(protocolNumber, keyword, description);
        numberToObject.put(protocolNumber, pn);
        if(keyword.length() > 0) keywordToObject.put(keyword.toLowerCase(), pn);
    }
    
    static {
        add(0,"HOPOPT","IPv6 Hop-by-Hop Option");
        add(1,"ICMP","Internet Control Message");
        add(2,"IGMP","Internet Group Management");
        add(3,"GGP","Gateway-to-Gateway");
        add(4,"IPv4","IPv4 encapsulation");
        add(5,"ST","Stream");
        add(6,"TCP","Transmission Control");
        add(7,"CBT","CBT");
        add(8,"EGP","Exterior Gateway Protocol");
        add(9,"IGP","any private interior gateway (used by Cisco for their IGRP)");
        add(10,"BBN-RCC-MON","BBN RCC Monitoring");
        add(11,"NVP-II","Network Voice Protocol");
        add(12,"PUP","PUP");
        add(13,"ARGUS","ARGUS (deprecated)");
        add(14,"EMCON","EMCON");
        add(15,"XNET","Cross Net Debugger");
        add(16,"CHAOS","Chaos");
        add(17,"UDP","User Datagram");
        add(18,"MUX","Multiplexing");
        add(19,"DCN-MEAS","DCN Measurement Subsystems");
        add(20,"HMP","Host Monitoring");
        add(21,"PRM","Packet Radio Measurement");
        add(22,"XNS-IDP","XEROX NS IDP");
        add(23,"TRUNK-1","Trunk-1");
        add(24,"TRUNK-2","Trunk-2");
        add(25,"LEAF-1","Leaf-1");
        add(26,"LEAF-2","Leaf-2");
        add(27,"RDP","Reliable Data Protocol");
        add(28,"IRTP","Internet Reliable Transaction");
        add(29,"ISO-TP4","ISO Transport Protocol Class 4");
        add(30,"NETBLT","Bulk Data Transfer Protocol");
        add(31,"MFE-NSP","MFE Network Services Protocol");
        add(32,"MERIT-INP","MERIT Internodal Protocol");
        add(33,"DCCP","Datagram Congestion Control Protocol");
        add(34,"3PC","Third Party Connect Protocol");
        add(35,"IDPR","Inter-Domain Policy Routing Protocol");
        add(36,"XTP","XTP");
        add(37,"DDP","Datagram Delivery Protocol");
        add(38,"IDPR-CMTP","IDPR Control Message Transport Proto");
        add(39,"TP++","TP++ Transport Protocol");
        add(40,"IL","IL Transport Protocol");
        add(41,"IPv6","IPv6 encapsulation");
        add(42,"SDRP","Source Demand Routing Protocol");
        add(43,"IPv6-Route","Routing Header for IPv6");
        add(44,"IPv6-Frag","Fragment Header for IPv6");
        add(45,"IDRP","Inter-Domain Routing Protocol");
        add(46,"RSVP","Reservation Protocol");
        add(47,"GRE","Generic Routing Encapsulation");
        add(48,"DSR","Dynamic Source Routing Protocol");
        add(49,"BNA","BNA");
        add(50,"ESP","Encap Security Payload");
        add(51,"AH","Authentication Header");
        add(52,"I-NLSP","Integrated Net Layer Security  TUBA");
        add(53,"SWIPE","IP with Encryption (deprecated)");
        add(54,"NARP","NBMA Address Resolution Protocol");
        add(55,"MOBILE","IP Mobility");
        add(56,"TLSP","Transport Layer Security Protocol using Kryptonet key management");
        add(57,"SKIP","SKIP");
        add(58,"IPv6-ICMP","ICMP for IPv6");
        add(59,"IPv6-NoNxt","No Next Header for IPv6");
        add(60,"IPv6-Opts","Destination Options for IPv6");
        add(61,"","any host internal protocol");
        add(62,"CFTP","CFTP");
        add(63,"","any local network");
        add(64,"SAT-EXPAK","SATNET and Backroom EXPAK");
        add(65,"KRYPTOLAN","Kryptolan");
        add(66,"RVD","MIT Remote Virtual Disk Protocol");
        add(67,"IPPC","Internet Pluribus Packet Core");
        add(68,"","any distributed file system");
        add(69,"SAT-MON","SATNET Monitoring");
        add(70,"VISA","VISA Protocol");
        add(71,"IPCV","Internet Packet Core Utility");
        add(72,"CPNX","Computer Protocol Network Executive");
        add(73,"CPHB","Computer Protocol Heart Beat");
        add(74,"WSN","Wang Span Network");
        add(75,"PVP","Packet Video Protocol");
        add(76,"BR-SAT-MON","Backroom SATNET Monitoring");
        add(77,"SUN-ND","SUN ND PROTOCOL-Temporary");
        add(78,"WB-MON","WIDEBAND Monitoring");
        add(79,"WB-EXPAK","WIDEBAND EXPAK");
        add(80,"ISO-IP","ISO Internet Protocol");
        add(81,"VMTP","VMTP");
        add(82,"SECURE-VMTP","SECURE-VMTP");
        add(83,"VINES","VINES");
        // http://www.dslreports.com/forum/r28704884-IPv4-Protocol-84-Question-why-duplicates
        // add(84,"TTP","Transaction Transport Protocol");
        add(84,"IPTM","Internet Protocol Traffic Manager");
        add(85,"NSFNET-IGP","NSFNET-IGP");
        add(86,"DGP","Dissimilar Gateway Protocol");
        add(87,"TCF","TCF");
        add(88,"EIGRP","EIGRP");
        add(89,"OSPFIGP","OSPFIGP");
        add(90,"Sprite-RPC","Sprite RPC Protocol");
        add(91,"LARP","Locus Address Resolution Protocol");
        add(92,"MTP","Multicast Transport Protocol");
        add(93,"AX.25","AX.25 Frames");
        add(94,"IPIP","IP-within-IP Encapsulation Protocol");
        add(95,"MICP","Mobile Internetworking Control Protocol (deprecated)");
        add(96,"SCC-SP","Semaphore Communications Sec. Protocol");
        add(97,"ETHERIP","Ethernet-within-IP Encapsulation");
        add(98,"ENCAP","Encapsulation Header");
        add(99,"","any private encryption scheme");
        add(100,"GMTP","GMTP");
        add(101,"IFMP","Ipsilon Flow Management Protocol");
        add(102,"PNNI","PNNI over IP");
        add(103,"PIM","Protocol Independent Multicast");
        add(104,"ARIS","ARIS");
        add(105,"SCPS","SCPS");
        add(106,"QNX","QNX");
        add(107,"A/N","Active Networks");
        add(108,"IPComp","IP Payload Compression Protocol");
        add(109,"SNP","Sitara Networks Protocol");
        add(110,"Compaq-Peer","Compaq Peer Protocol");
        add(111,"IPX-in-IP","IPX in IP");
        add(112,"VRRP","Virtual Router Redundancy Protocol");
        add(113,"PGM","PGM Reliable Transport Protocol");
        add(114,"","any 0-hop protocol");
        add(115,"L2TP","Layer Two Tunneling Protocol");
        add(116,"DDX","D-II Data Exchange (DDX)");
        add(117,"IATP","Interactive Agent Transfer Protocol");
        add(118,"STP","Schedule Transfer Protocol");
        add(119,"SRP","SpectraLink Radio Protocol");
        add(120,"UTI","UTI");
        add(121,"SMP","Simple Message Protocol");
        add(122,"SM","Simple Multicast Protocol (deprecated)");
        add(123,"PTP","Performance Transparency Protocol");
        add(124,"ISIS over IPv4","ISIS over IPv4");
        add(125,"FIRE","FIRE");
        add(126,"CRTP","Combat Radio Transport Protocol");
        add(127,"CRUDP","Combat Radio User Datagram");
        add(128,"SSCOPMCE","SSCOPMCE");
        add(129,"IPLT","IPLT");
        add(130,"SPS","Secure Packet Shield");
        add(131,"PIPE","Private IP Encapsulation within IP");
        add(132,"SCTP","Stream Control Transmission Protocol");
        add(133,"FC","Fibre Channel");
        add(134,"RSVP-E2E-IGNORE","RSVP-E2E-IGNORE");
        add(135,"Mobility Header","Mobility Header");
        add(136,"UDPLite","UDPLite");
        add(137,"MPLS-in-IP","MPLS-in-IP");
        add(138,"manet","MANET Protocols");
        add(139,"HIP","Host Identity Protocol");
        add(140,"Shim6","Shim6 Protocol");
        add(141,"WESP","Wrapped Encapsulating Security Payload");
        add(142,"ROHC","Robust Header Compression");
        for (int i=143; i<=252; i++) { add(i,"","Unassigned"); }
        add(253,"","Use for experimentation and testing");
        add(254,"","Use for experimentation and testing");
        add(255,"Reserved","");
    }
}
