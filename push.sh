#!/bin/bash
#

SERVER=il-pg-alpha-87904.aps2.palantir.global
PORT=6565
USER=palantir
PASS=palantir


createDS() {
  DATASET=$2
  echo create new dataset: $DATASET
  curl -ku $USER:$PASS -XPOST https://$SERVER:$PORT/v8/datasets/$DATASET
}

newTX() {
  DATASET=$2
  DSMETAFILE=$3
  echo -e '\n\n'create new transaction, including meta file: $DSMETAFILE
  TXRESPONSE=$(curl -ku $USER:$PASS -XPOST https://$SERVER:$PORT/v8/datasets/$DATASET/transaction --data-binary @$DSMETAFILE )
  # echo Transaction response: $TXRESPONSE
  # JSON parsing with regex is not a recommended approach, but thats how I roll. #noDepenencies
  TRANSACTION=$(echo $TXRESPONSE | grep -Eo '"transactionId":"[0-9]+"' | grep -Eo '[0-9]+' )
  if [ -z $TRANSACTION ]; then
    echo Transaction failed: $TXRESPONSE
    exit
  fi
  echo ""
  echo transaction id is: $TRANSACTION
}

push() {
  DATASET=$2
  TRANSACTION=$3
  DSFILE=$4
  echo push datafile: $DSFILE
  curl -ku $USER:$PASS -XPOST https://$SERVER:$PORT/v8/transactions/$DATASET/$TRANSACTION/main/file/$DSFILE -T $DSFILE
  
}

closeTX() {
  DATASET=$2
  TRANSACTION=$3
  echo close transaction
  curl -ku $USER:$PASS -XPUT https://$SERVER:$PORT/v8/transactions/$DATASET/$TRANSACTION
  
}
### main logic ###
case "$1" in
  createDS)
        createDS "$@"
        ;;
  newTX)
        newTX "$@"
        ;;
  push)
        push "$@"
        ;;
  closeTX)
        closeTX "$@"
        ;;
  *)
        echo $"Usage: $0 {createDS <ds>|newTX <ds> <metaFile>|push <ds> <tx> <file>|closeTX <ds> <tx>}"
        exit 1
esac
exit 0